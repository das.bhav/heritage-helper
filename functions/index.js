'use strict'
const functions = require('firebase-functions');
const {dialogflow, Image, Button, BasicCard} = require('actions-on-google');
const app = dialogflow();
const locations = {
    'Principal Office'          : 'CME Ground Floor',
    'Admin Office'              : 'CME Ground Floor',
    'TPO Office'                : 'CME 003, Ground Floor',
    'CSE HOD'                   : 'ICT 3rd Floor, Just beside the Stairs',
    'CSE Faculty Room'          : 'CB 6th Floor',
    'CSE Department'            : 'ICT 3rd Floor',
    'ECE HOD'                   : 'ICT 4th Floor, Just beside the Stairs',
    'ECE Faculty Room'          : 'CB 5TH FLOOR',
    'ECE Department'            : 'ICT 4TH FLOOR',
    'IT HOD'                    : 'ICT 2nd Floor, Just beside the Stairs',
    'IT Faculty Room'           : 'ICT 2nd Floor',
    'IT Department'             : 'ICT 2nd FLOOR',
    'Chemical HOD'              : 'CB 2nd FLOOR',
    'Chemical Faculty Room'     : 'CB 209',
    'Chemical Department'       : 'CB 2nd FLOOR',
    'Physics HOD'               : 'CB 1st Floor',
    'Physics Faculty Room'      : 'CB 1ST FLOOR',
    'Physics Department'        : 'CB 1ST FLOOR',
    'Math HOD'                  : 'ICT 6TH FLOOR',
    'Math Faculty Room'         : 'ICT 6th FLOOR',
    'Math Department'           : 'ICT 6TH FLOOR',
    'HMTS HOD'                  : 'ICT 2nd Floor',
    'HMTS Faculty Room'         : 'ICT 2nd Floor',
    'HMTS Department'           : 'In your own class',
    'Mechanical HOD'            : 'CME 1st Floor',
    'Mechanical Faculty Room'   : 'CME 1st Floor',
    'Mechanical Department'     : 'CME 1st Floor',
    'Civil HOD'                 : 'CME 2nd Floor',
    'Civil Faculty Room'        : 'CME 2nd Floor',
    'Civil Department'          : 'CME 2nd Floor',
    'Electrical HOD'            : 'CME 4th Floor',
    'Electrical Faculty Room'   : 'CME 4th Floor',
    'Electrical Department'     : 'CME 4th and 5th Floors',
    'AEIE HOD'                  : 'ICT 2nd FLoor',
    'AEIE Department'           : 'ICT 2nd Floor',
    'AEIE Faculty Room'         : 'ICT 2nd Floor',
    'Mechanical Workshop'        : 'CME Basement',

};

app.intent('Where is', (conv, {Location}) =>{
    console.log(Location);
    console.log(locations[Location]);
	const requiredLocation = locations[Location];
	conv.close(`You can find ${Location} at ${requiredLocation}`);
});

app.intent('How To', (conv, {Activity})=> {
    console.log(Activity);
    let result;
    if(Activity === 'Join DSC'){
        conv.ask('Sure! Welcome to the Family!');
        if (!conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT')) {
            conv.ask('Sorry, try this on a screen device');
            return;
        }
        result = new BasicCard({
                text: 'Amazing! At Developer Student\'s club, we learn together and '+
                'implement our learnings in building many amazing projects'+ 'Simply introduce yourself to this'+
                ' WhatsApp Group and we will guide you through the rest!',
                buttons: new Button({
                    title: 'DSC WhatsApp Group',
                    url: 'https://chat.whatsapp.com/2QXwM5bteaG0cy2eOdDXyI',
                }),
                image: new Image({
                    url: 'https://image.ibb.co/jSb1G8/4d9c3e22_8489_4e52_bb47_1f12cb61436b.jpg',
                    alt: 'Developer Students Club India',
                }),
            });
        // result = 'Introduce yourself to our WhatSapp group, we will take care of the rest:'+
        // 'https://chat.whatsapp.com/2QXwM5bteaG0cy2eOdDXyI';
    }
    else{
        result = 'I really don\'t know yet. Will try to do better next time.';
    }
    console.log(result);
    conv.close(result);
});
exports.dialogflowFirebaseFulfillment = functions.https.onRequest(app);


